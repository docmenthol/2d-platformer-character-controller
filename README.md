# 2D Platformer Character Controller

A fancy (and advanced) Godot character controller for 2D platformers. The
primary goal of this character controller is a tunable, projectile motion based
jump as well as a set of common features that can be toggled and tweaked to fit
just about any platformer game's needs.